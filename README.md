## PROYECTO DJANGO - Pasos

* Crear un proyecto con el comando "django-admin startproject nombreproyecto"
* Abrir la carpeta del proyecto con un editor de código
* Desde el editor, ejecutar el comando "python manage.py startapp app"
* Dentro de la carpeta del proyecto, crear una nueva carpeta con el nombre "templates"
* Editar archivo "nombreproyecto/settings.py"
```
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        (*) 'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    (*) 'app',
]
```
* Editar "nombreproyecto/urls.py", incluir "path('', include('app.urls'))" 
```
from django.contrib import admin
from django.urls import path
from django.urls.conf import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app.urls')),
]
```
* En app, crear el archivo "urls.py"
* En app, agregar los modelos en "models.py"
* En "app/admin.py" registrar los modelos
* Ejecutar "python manage.py makemigrations" y "python manage.py migrate"
* Crear superusuario para ingresar a la base de datos con el comando "python manage.py createsuperuser"
* Ejecutar "python manage.py runserver" 
* Ingresar desde el navegador a "http://localhost:8000/admin/login/" e ingresar el user y password creados anteriormente
* En la carpeta "templates", crear los archivos HTML
* Editar "views.py" de "app" y escribir las funciones

# TUTORIALES

* Canal: https://www.youtube.com/c/PythonBricks/videos
* Foreign Key: https://www.youtube.com/watch?v=4m39miuw5lE
* Personalización del Admin de Django: https://www.youtube.com/watch?v=c5uV8JWLovc

* CRUD Completo: https://www.youtube.com/watch?v=N6jzspc2kds
* Generar PDF: https://www.youtube.com/watch?v=N9iQm4N3H8s